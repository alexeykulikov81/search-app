export class DummySearchEngine {
  constructor(data) {
    this.data = data
  }

  countPrefix(prefix) {
    let res = this.data.filter((item) => item.text.includes(prefix))
    return res.length
  }
}

export class RegexpSearchEngine {
  constructor(data) {
    this.data = data
  }

  countPrefix(prefix) {
    let pattern = `^${prefix}`
    let re = new RegExp(pattern.toUpperCase())
    let result = this.data.filter((item) => re.exec(item.text.toUpperCase()))
    return result.length
  }
}

export class TrieSearchEngine {
  constructor(myArr = []) {
    this.root = {}
    myArr.forEach((e) => this.insert(e))
  }
  insert(query) {
    let node = this.root
    for (const c of Object.values(query.text.toUpperCase())) {
      if (!node[c]) node[c] = {}
      node = node[c]
      
    }
    node.end = true
  }

  countPrefix(prefix) {
    let node = this.searchNode(prefix)
    if (node === null) {
      return 0
    }
    // return current.end
    return this.countChildrenWords(node)
  }

  searchNode(query) {
    let current = this.root
    for(let c of query.toUpperCase()){
      if(current[c] === undefined){
        return null
      }
      current = current[c]
    }
    return current
  }

  countChildrenWords(node) {
    let count = 0
    if (node.end !== undefined) {
      count++
    }
    for (let letter in node) {
        count += this.countChildrenWords(node[letter])
    }
    return count
  }

}
