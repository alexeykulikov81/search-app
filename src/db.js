// IndexedDB wrapper class
export class IndexedDB {

    // connect to IndexedDB database
    constructor(dbName, dbVersion, dbUpgrade) {
  
      return new Promise((resolve, reject) => {
  
        // connection object
        this.db = null;
  
        // no support
        if (!('indexedDB' in window)) reject('not supported');
  
        // open database
        const dbOpen = indexedDB.open(dbName, dbVersion);
  
        if (dbUpgrade) {
  
          // database upgrade event
          dbOpen.onupgradeneeded = e => {
            dbUpgrade(dbOpen.result, e.oldVersion, e.newVersion);
          };
        }
  
        dbOpen.onsuccess = () => {
          this.db = dbOpen.result;
          resolve( this );
        };
  
        dbOpen.onerror = e => {
          reject(`IndexedDB error: ${ e.target.errorCode }`);
        };
  
      });
  
    }
  }