let idCounter = 0

export function generateData(count) {
  const data = []

  var abc =
    'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя'
  var random = abc[Math.floor(Math.random() * abc.length)]

  for (let index = 0; index < count; index++) {
    let newAbc = ''
    while (newAbc.length < 20) {
      random = abc[Math.floor(Math.random() * abc.length)]
      newAbc += random
    }

    data.push({
      id: String(idCounter++),
      text: newAbc,
    })
  }

  return data
}
