import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

import VirtualList from "vue-virtual-scroll-list";

Vue.component("virtual-list", VirtualList);

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
